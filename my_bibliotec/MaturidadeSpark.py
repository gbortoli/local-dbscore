# Código para fazer a análise da Maturidade dos dados usando Py Spark (para grandes quantidades de dados)

import warnings
import os
import chardet
import csv
import psutil
import datetime
from pyspark.sql import SparkSession
from pyspark import StorageLevel
import pyspark.sql.functions as F
from pyspark.sql.functions import col, when, count
from pyspark.sql.types import StringType, IntegerType, FloatType, DoubleType

########################################################################################################################

warnings.simplefilter("ignore", UserWarning)

########################################################################################################################

debugMode = False

########################################################################################################################


def ValidarMaturidadeCriarSample(csvPath):

    if debugMode:
        print("Start time: ", datetime.datetime.now().strftime("%H:%M:%S"))

    os.environ["JAVA_HOME"] = "C:/Program Files/Java/jdk-22"
    os.environ["PATH"] = os.environ["JAVA_HOME"] + "/bin:" + os.environ["PATH"]

    if debugMode:
        print("01 - Java environment set")

    #############################

    # Get system memory info
    mem_info = psutil.virtual_memory()

    # Get total and available memory (in GB)
    total_mem_gb = int(mem_info.total / (1024**3))

    # Distribute memory for executor, driver and onverhead; to be used in Spark
    if (total_mem_gb >= 15):
        mem_exec = f"{int((total_mem_gb-4)/2)}g"
        mem_driver = f"{int((total_mem_gb-2)/2)}g"
        mem_overhead = f"{int((total_mem_gb/8) + 1)}g"
    else:
        mem_exec = f"{int((total_mem_gb-1)/2)}g"
        mem_driver = f"{int((total_mem_gb-1)/2)}g"
        mem_overhead = f"{int((total_mem_gb/4) + 1)}g"

    if debugMode:
        print(f"Total memory: {total_mem_gb}GB | Executor: {mem_exec} | Driver: {mem_driver} | Overhead: {mem_overhead}\n")

    #############################

    separator_auto, encoding_auto = detect_separator_and_encoding(csvPath)

    if debugMode:
        print("02 - CSV separator and encoding detected")

    #############################

    # Initialize a SparkSession:
    spark = (
        SparkSession.builder.appName("CSV_Reader")
        .config("spark.executor.memory", mem_exec)
        .config("spark.driver.memory", mem_driver)
        .config("spark.executor.memoryOverhead", mem_overhead)
        .config("spark.executor.extraJavaOptions", "-XX:+UseG1GC")
        .getOrCreate()
    )

    if debugMode:
        print("03 - Spark Session created - Next: Read File")

    # Read the CSV file into a DataFrame
    pre_df = spark.read.csv(
        csvPath,
        sep=separator_auto,
        encoding=encoding_auto,
        header=True,
        inferSchema=True,
    )

    if debugMode:
        print("04 - File read")

    # Change some characters on Column names to avoid conflict with SQL queries - no impact on data at all.
    new_column_names = [c.replace(" ", "_").replace(".", "") for c in pre_df.columns]
    df = pre_df.toDF(*new_column_names)

    df.persist(
        StorageLevel.MEMORY_AND_DISK
    )  # Instead of persist, we could use df.cache(). It's a bit quicker, but gives more warnings.

    columns = df.columns

    total_rows = df.count()
    total_cols = len(columns)
    total_fields = total_rows * total_cols

    scoreMaturidade = []

    if debugMode:
        print(
            "05 - Preparation OK. Next: Score calculation \n",
            datetime.datetime.now().strftime("%H:%M:%S"),
            "\n",
        )

    # COMPLETUDE:
    completudeScore = CompletudeSpark(df, total_fields, columns)
    scoreMaturidade.append(completudeScore)

    if debugMode:
        print(
            "06 - Completeness calculation OK \n",
            datetime.datetime.now().strftime("%H:%M:%S"),
            "\n",
        )

    # SINGULARIDADE:
    singularidadeScore = SingularidadeSpark(df, columns, total_rows, spark)
    scoreMaturidade.append(singularidadeScore)

    if debugMode:
        print(
            "07 - Singularity calculation OK \n",
            datetime.datetime.now().strftime("%H:%M:%S"),
            "\n",
        )

    # VALIDADE:
    validadeScore = ValidadeSpark(df, total_fields, columns)
    scoreMaturidade.append(validadeScore)

    if debugMode:
        print(
            "08 - Validity calculation OK \n",
            datetime.datetime.now().strftime("%H:%M:%S"),
            "\n",
        )

    # CONSISTENCIA:
    consistenciaScore = ConsistenciaSpark(df, total_fields)
    scoreMaturidade.append(consistenciaScore)

    if debugMode:
        print(
            "09 - Consistency OK \n", datetime.datetime.now().strftime("%H:%M:%S"),
            "\n"
        )

    # Create a sample (fraction parameter [with ~100 rows] should be set based on the size of the original dataset)
    sample_df = df.sample(False, 100 / total_rows).toPandas()

    if debugMode:
        print(
            "10 - Sample created for FAIR and Openness \n",
            datetime.datetime.now().strftime("%H:%M:%S"),
            "\n\n",
        )

    df.unpersist()  ###################################################################

    if debugMode:
        print("End time: ", datetime.datetime.now().strftime("%H:%M:%S"))

    return scoreMaturidade, sample_df


########################################################################################################################


def CompletudeSpark(df, total_fields, columns):

    # Count the number of null values in each column
    null_counts = df.select([count(when(col(c).isNull(), c)).alias(c) for c in columns])

    null_counts_row = null_counts.collect()[0].asDict()
    total_missing = sum(list(null_counts_row.values()))

    completudeScore = round((1 - (total_missing / total_fields)) * 100, 2)

    return completudeScore


def SingularidadeSpark(df, columns, total_rows, spark):

    # Register the DataFrame as a temporary SQL table
    df.createOrReplaceTempView("validation_table")

    # Build the concatenation string using concat_ws with a delimiter (e.g., '|') - This step is necessary to compare the entries and check for duplicates.
    concat_expr = f"CONCAT_WS('|', {', '.join([f'`{col}`' for col in columns])})"

    # Build the SQL query to count distinct concatenated values
    query = (
        f"SELECT COUNT(DISTINCT {concat_expr}) AS unique_entries FROM validation_table"
    )

    # Execute the SQL query
    unique_count = spark.sql(query)

    # Get the result:
    result = unique_count.collect()

    # Extract the number from the result
    unique_entries = result[0]["unique_entries"]

    # Singularity Check:
    singularidadeScore = round((unique_entries / total_rows) * 100, 2)

    return singularidadeScore


def ValidadeSpark(df, total_fields, columns):

    # Analyze each column
    results = []
    for column in columns:
        column_type = df.schema[column].dataType
        if isinstance(column_type, (IntegerType, FloatType, DoubleType)):
            anomaly_count = detect_numeric_anomalies(df, column)
        elif isinstance(column_type, StringType):
            anomaly_count = detect_text_anomalies(df, column)
        else:
            continue  # Skip columns with other data types

        anomaly_count = anomaly_count if anomaly_count is not None else 0
        results.append((column, anomaly_count))

    # Get results
    total_anomaly_count = sum(anomaly_count for _, anomaly_count in results)

    validadeScore = round((1 - (total_anomaly_count / total_fields)) * 100, 2)

    return validadeScore


def ConsistenciaSpark(df, total_fields):

    elementos_corretos = 0  # Contador para elementos corretos

    for coluna in df.columns:
        tipo_original = df.schema[coluna].dataType

        if isinstance(tipo_original, StringType):
            # Verificar se todos os valores são strings
            elementos_corretos += df.filter(
                F.col(coluna).cast(StringType()).isNotNull()
            ).count()
        elif isinstance(tipo_original, (IntegerType, FloatType, DoubleType)):
            # Verificar se todos os valores são numéricos
            elementos_corretos += df.filter(
                F.col(coluna).cast(DoubleType()).isNotNull()
            ).count()

    # Calcular a porcentagem de dados corretos
    consistenciaScore = round(((elementos_corretos / total_fields) * 100), 2)

    return consistenciaScore


########################################################################################################################


def detect_numeric_anomalies(df, column):

    global debugMode

    # Filter out NULL values and get only the necessary column:
    df_filtered = df.select(column).filter(col(column).isNotNull())
    num_rows = df_filtered.count()

    # Part 1: Inter-quartile difference
    quantiles = df_filtered.approxQuantile(column, [0.25, 0.75], 0.05)
    q1, q3 = quantiles
    iqr = q3 - q1
    lower_bound = q1 - 1.7 * iqr # 1.7 makes the result be equivalent to  ~6-sigma distribution, or 3.4 DPMO (defects per million opportunities)
    upper_bound = q3 + 1.7 * iqr

    # Part 2: Frequency
    freq_df = df_filtered.groupBy(column).count().withColumnRenamed("count", "frequency")
        
    # Count anomalies:
    num_cat = freq_df.count()
    min_frequency = num_rows * (1 / (num_cat * 5))

    anomalies_df = freq_df.filter(
        (col(column) < lower_bound) | (col(column) > upper_bound)
    ).filter(col("frequency") < min_frequency)

    anomaly_count = anomalies_df.agg({"frequency": "sum"}).collect()[0][0]

    if debugMode:
        print(f"Column: {column} - Anomaly count Number: {anomaly_count}")

    return anomaly_count


def detect_text_anomalies(df, column):

    global debugMode

    # Filter out NULL values and get only the necessary column:
    df_filtered = df.select(column).filter(col(column).isNotNull())
    num_rows = df_filtered.count()

    # Assuming anomalies are less frequent values
    freq_df = df.groupBy(column).count().withColumnRenamed("count", "frequency")

    # Count anomalies:
    num_cat = freq_df.count()
    min_frequency = num_rows * (1 / (num_cat * 5))

    anomalies_df = freq_df.filter(col("frequency") < min_frequency)
    anomaly_count = anomalies_df.agg({"frequency": "sum"}).collect()[0][0]

    if debugMode:
        print(f"Column: {column} - Anomaly count Text: {anomaly_count}")

    return anomaly_count


########################################################################################################################


def detect_separator_and_encoding(file_path):
    with open(file_path, "rb") as file:
        raw_data = file.read(8192)  # Read the first 8KB of the file

        # Detect encoding
        result = chardet.detect(raw_data)
        encoding = result["encoding"]
        if encoding.upper() == "UTF-8-SIG":
            encoding = "UTF-8"

        # Read as text to check separators
        text = raw_data.decode(encoding)

        # Detect separator
        sniffer = csv.Sniffer()
        dialect = sniffer.sniff(text)
        separator = dialect.delimiter

    return separator, encoding


########################################################################################################################
